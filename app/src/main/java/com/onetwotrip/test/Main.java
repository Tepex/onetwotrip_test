package com.onetwotrip.test;

import java.io.IOException;

import java.lang.RuntimeException;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main
{
	private static final String BASE = "OneTwoTrip";
	private static final int MAX = 10000;
	private static final Pattern DIMENSION = Pattern.compile("^(\\d+)\\s+(\\d+)$");
	private static final boolean DEBUG = false;
	
	public static void main(String[] args)
	{
		if(args.length == 0)
		{
			System.err.println("Usage: ./app input_file");
			return;
		}
		Path path = Paths.get(args[0]);
		if(Files.notExists(path))
		{
			System.err.println("Source file "+path+" does not exists");
			return;
		}
		
		List<String> lines;
		try
		{
			lines = Files.readAllLines(path, Charset.defaultCharset());
		}
		catch(IOException e)
		{
			System.err.println("Can\'t read file "+path);
			return;
		}
		if(lines.size() == 0)
		{
			System.err.println("File "+path+" is empty!");
			return;
		}
		
		int n = -1, m = -1;
		// Парсим первую строку для получения размерности матрицы; 
		Matcher matcher = DIMENSION.matcher(lines.get(0));
		if(matcher.matches())
		{
			try
			{
				m = Integer.parseInt(matcher.group(1));
				n = Integer.parseInt(matcher.group(2));
			}
			catch(RuntimeException e) {}
		}
		if(n < 0 || n > MAX || m < 0 || m > MAX)
		{
			System.err.println("Wrong file format. First string must be two numbers (0 - 10 000) separated by space.");
			return;
		}
		if(DEBUG) System.out.println("n = "+n+" m = "+m);
		if(m >= lines.size())
		{
			System.err.println("Wrong file format! The number of lines less than "+m);
			return;
		}
		
		char[] compareBase = BASE.toLowerCase().toCharArray();
		
		// Инициализация выходного массива. out[r][0] = -1 означает, что символ BASE[r] еще не найден.
		// out[r][0] - индекс строки, out[r][1] - индекс столбца найденного символа.
		int[][] out = new int[compareBase.length][2];
		for(int r = 0; r < out.length; ++r) out[r][0] = -1;
		
		int found = 0; // кол-во найденных символов в compareBase
		// поиск символа из матрицы в строке compareBASE
		for(int r = 0; r < m; ++r)
		{
			String line = lines.get(r+1).toLowerCase();
			for(int c = 0; c < n || c < line.length(); ++c)
			{
				char nextChar = line.charAt(c);
				if(DEBUG) System.out.println("("+r+","+c+") nextChar = "+nextChar);
				for(int i = 0; i < out.length; ++i)
				{
					if(DEBUG) System.out.println("out["+i+"]="+out[i][0]+" nextChar="+nextChar+" compareBase["+i+"]="+compareBase[i]);
					// ищем следующий ненайденный символ
					if(out[i][0] == -1 && nextChar == compareBase[i])
					{
						out[i][0] = r;
						out[i][1] = c;
						if(DEBUG)
						{
							System.out.print("Bingo! ("+r+","+c+") {");
							for(int a = 0; a < out.length; ++a)
							{
								if(out[a][0] != -1) System.out.print(BASE.charAt(a));
								else System.out.print("-");
								if(a < out.length-1) System.out.print(", ");
							}
							System.out.println("} found = "+(found+1));
						}
						if(++found == compareBase.length)  // найдены все символы
						{
							for(int k = 0; k < out.length; ++k) System.out.println(BASE.charAt(k)+" - ("+out[k][0]+", "+out[k][1]+")");
							return;
						}
						break;
					}
				}
				if(DEBUG)System.out.println("------");
			}
			if(DEBUG) System.out.println("===============");
		}
		System.out.println("Impossible");
	}
}